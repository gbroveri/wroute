# README #

### What is this repository for? ###
This is a api for getting the best route between a source and a destination.
It's based on [Dijkstra's algorithm](http://en.wikipedia.org/wiki/Dijkstra's_algorithm).

## Architecture ##
![wroute.png](https://bitbucket.org/repo/L79Xqa/images/737904374-wroute.png)

### Technologies ###
* This is a Java Maven project
* It uses Spring MVC on the web layer
* It uses Spring Transactional Services on business layer
* It uses JPA (Hibernate implementation) on the data access layer
* Integration and unit tests are implemented with Cucumber JVM
* Hibernate validator (JSR 303) is used to validate business rules

## Scenarios ##
* Scenarios covered can be found [here](http://gbroveri.bitbucket.org/wroute/#Test_Cases)

### Database configuration ###
* HsqlDb is set as the database.
>It can be easily changed through the properties files:

* persistence_dev.properties (Development environment properties).
>It uses in memory java hsql db.

* persistence_test.properties (Test environment properties).  
>It uses in memory java hsql db.

* persistence_prod.properties (Production environment properties).
>It uses file system java hsql db.

### Pre Requisites ###
* Git
* JDK 1.7
* Maven
  
### Running locally ###
> mvn -Denv=dev jetty:run

### How to run tests ###
> mvn -Denv=test test

### Deployment instructions ###
> mvn -Denv=prod package

## Api methods ##
### Creating new logistic map - example: ###
* http POST - http://54.214.94.224:8080/wroute/api/routes
* json body:
```
#!json
{
    "name": "mapa inicial",
    "routes": [
        {
            "source": "A",
            "destination": "B",
            "distance": 10
        },
        {
            "source": "B",
            "destination": "D",
            "distance": 15
        },
        {
            "source": "A",
            "destination": "C",
            "distance": 20
        },
        {
            "source": "C",
            "destination": "D",
            "distance": 30
        },
        {
            "source": "B",
            "destination": "E",
            "distance": 50
        },
        {
            "source": "D",
            "destination": "E",
            "distance": 30
        }
    ]
}
```
* returns http 200 with success message.

### Find best route ###

* http GET - http://54.214.94.224:8080/wroute/api/routes?source=A&destination=D&consumption=10&fuelPrice=2.50

* returns:
```
#!json
{
    "route": [
        "A",
        "B",
        "D"
    ],
    "value": 6.25
}
```