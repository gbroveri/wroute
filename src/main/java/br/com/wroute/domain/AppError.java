package br.com.wroute.domain;

/**
 * Created by gbroveri.
 */
public class AppError implements Domain {
    private String code;
    private Object[] params;
    private String field;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AppError)) return false;

        AppError error = (AppError) o;

        if (field != null ? !field.equals(error.field) : error.field != null) return false;
        if (message != null ? !message.equals(error.message) : error.message != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = field != null ? field.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}
