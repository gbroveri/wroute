package br.com.wroute.domain;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by gbroveri.
 */
public class RouteCost implements Domain {
    private List<String> route;
    private BigDecimal value;

    public RouteCost() {

    }

    public RouteCost(List<String> route, BigDecimal value) {
        this.route = route;
        this.value = value;
    }

    public List<String> getRoute() {
        return route;
    }

    public void setRoute(List<String> route) {
        this.route = route;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
