package br.com.wroute.domain;

import br.com.wroute.domain.validator.ValidSourceAndDestination;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by gbroveri.
 */
@ValidSourceAndDestination(message = "route.same.source.and.destination")
public class Route implements Domain {

    private Long id;

    @NotBlank(message = "route.source.blank")
    private String source;

    @NotBlank(message = "route.destination.blank")
    private String destination;

    @NotNull(message = "route.distance.nullable")
    @Min(value = 1, message = "route.distance.min.tooSmall")
    private Integer distance;

    private Long mapId;

    public Route() {
        //
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMapId() {
        return mapId;
    }

    public void setMapId(Long mapId) {
        this.mapId = mapId;
    }

}
