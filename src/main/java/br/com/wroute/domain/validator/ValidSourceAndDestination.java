package br.com.wroute.domain.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by gbroveri.
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidSourceAndDestinationValidator.class})
@Documented
public @interface ValidSourceAndDestination {

    String message() default "{route.same.source.and.destination}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

