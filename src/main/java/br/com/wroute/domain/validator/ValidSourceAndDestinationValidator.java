package br.com.wroute.domain.validator;

import br.com.wroute.domain.Route;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by gbroveri.
 */
public class ValidSourceAndDestinationValidator implements ConstraintValidator<ValidSourceAndDestination, Route> {
    @Override
    public void initialize(ValidSourceAndDestination validSourceAndDestination) {
    }

    @Override
    public boolean isValid(Route route, ConstraintValidatorContext constraintValidatorContext) {
        if (route == null || route.getSource() == null || route.getDestination() == null)
            return true;
        return !route.getSource().equalsIgnoreCase(route.getDestination());
    }
}
