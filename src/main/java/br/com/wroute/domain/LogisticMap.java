package br.com.wroute.domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.Collection;

/**
 * Created by gbroveri.
 */
public class LogisticMap implements Domain {

    private Long id;

    @NotBlank(message = "map.name.blank")
    private String name;

    @Valid
    @NotEmpty(message = "map.routes.empty")
    private Collection<Route> routes;

    public LogisticMap() {
        //
    }

    public LogisticMap(String name, Collection<Route> routes) {
        this.name = name;
        this.routes = routes;
    }

    public String getName() {
        return name;
    }

    public Collection<Route> getRoutes() {
        return routes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoutes(Collection<Route> routes) {
        this.routes = routes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
