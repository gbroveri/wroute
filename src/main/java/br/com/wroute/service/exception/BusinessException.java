package br.com.wroute.service.exception;

import br.com.wroute.domain.AppError;
import br.com.wroute.domain.Domain;

import javax.validation.ConstraintViolation;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by gbroveri.
 */
public class BusinessException extends Exception {
    private final Set<? extends ConstraintViolation<? extends Domain>> violations;
    private final Set<AppError> errors;

    public BusinessException(Set<? extends ConstraintViolation<? extends Domain>> violations) {
        this.violations = violations;
        errors = new HashSet<>();
        for (ConstraintViolation<? extends Domain> violation : this.violations) {
            final AppError error = new AppError();
            error.setCode(violation.getMessageTemplate());
            error.setMessage(violation.getMessage());
            if (violation.getPropertyPath() != null)
                error.setField(violation.getPropertyPath().toString());
            if (violation.getExecutableParameters() != null)
                error.setParams(violation.getExecutableParameters());

            this.errors.add(error);
        }
    }

    public Set<? extends ConstraintViolation<? extends Domain>> getViolations() {
        return violations;
    }

    public Set<AppError> getErrors() {
        return errors;
    }
}
