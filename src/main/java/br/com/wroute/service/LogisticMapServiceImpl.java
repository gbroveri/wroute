package br.com.wroute.service;

import br.com.wroute.domain.ConstraintError;
import br.com.wroute.domain.LogisticMap;
import br.com.wroute.domain.Route;
import br.com.wroute.domain.RouteCost;
import br.com.wroute.service.dijkstra.Dijkstra;
import br.com.wroute.service.dijkstra.Edge;
import br.com.wroute.service.dijkstra.Vertex;
import br.com.wroute.service.exception.BusinessException;
import br.com.wroute.service.it.LogisticMapDao;
import br.com.wroute.service.it.LogisticMapService;
import org.apache.commons.lang3.Validate;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by gbroveri.
 */
@Service
@Transactional(rollbackFor = BusinessException.class)
public class LogisticMapServiceImpl implements LogisticMapService {

    private static Logger log = Logger.getLogger(LogisticMapServiceImpl.class);

    @Autowired
    LogisticMapDao mapDao;

    @Autowired
    LocalValidatorFactoryBean validatorFactory;

    public LogisticMap create(LogisticMap newMap) throws BusinessException {
        LogisticMap retval = null;
        final Validator validator = validatorFactory.getValidator();
        final Set<ConstraintViolation<LogisticMap>> violations = validator.validate(newMap);
        if (!violations.isEmpty())
            throw new BusinessException(violations);
        try {
            retval = mapDao.create(newMap);
        } catch (PersistenceException e) {
            log.error(e.getMessage(), e);
            if (e.getCause() instanceof ConstraintViolationException) {
                Set<ConstraintViolation<LogisticMap>> errorSet = new HashSet<>();
                ConstraintError<LogisticMap> error = new ConstraintError<LogisticMap>();
                error.setMessage("map.name.unique");
                error.setMessageTemplate("map.name.unique");
                errorSet.add(error);
                throw new BusinessException(errorSet);
            }
        }
        return retval;
    }

    @Override
    public RouteCost findRouteCost(String source, String destination, Integer consumption, BigDecimal fuelPrice) throws BusinessException {
        Validate.notBlank(source);
        Validate.notBlank(destination);
        Validate.notNull(consumption);
        Validate.notNull(fuelPrice);
        Validate.isTrue(consumption > 0);
        Validate.isTrue(fuelPrice.compareTo(BigDecimal.ZERO) > 0);
        final List<Vertex> vertexList = getShortestPath(source, destination);
        if (vertexList != null && vertexList.size() > 1) {
            List<String> routes = new ArrayList<>();
            for (Vertex vertex : vertexList)
                routes.add(vertex.getName());
            final Integer distance = vertexList.get(vertexList.size() - 1).getMinDistance();
            return new RouteCost(routes, calculateCost(distance, consumption, fuelPrice));
        } else {
            Set<ConstraintViolation<Route>> errorSet = new HashSet<>();
            ConstraintError<Route> error = new ConstraintError<>();
            error.setMessage("route.not.found");
            error.setMessageTemplate("route.not.found");
            errorSet.add(error);
            throw new BusinessException(errorSet);
        }
    }

    private List<Vertex> getShortestPath(String source, String destination) {
        Validate.notBlank(source);
        Validate.notBlank(destination);
        Dijkstra dijkstra = getDijkstra(convertToVertices(getRoutes()));
        dijkstra.loadDistances(source);
        final List<Vertex> vertexList = dijkstra.getShortestPathTo(destination);
        return vertexList;
    }

    Collection<Route> getRoutes() {
        return mapDao.findAllRoutes();
    }

    BigDecimal calculateCost(Integer distance, Integer consumption, BigDecimal fuelPrice) {
        double litres = ((double) distance / consumption);
        return fuelPrice.multiply(BigDecimal.valueOf(litres));
    }

    Dijkstra getDijkstra(Collection<Vertex> vertices) {
        return new Dijkstra(vertices);
    }

    private Collection<Vertex> convertToVertices(final Collection<Route> routes) {
        Validate.notEmpty(routes);
        final Map<String, Vertex> referencesCache = new HashMap<>();
        final Map<Vertex, Collection<Edge>> vertices = new HashMap<>();
        for (final Route route : routes) {
            Vertex source = referencesCache.get(route.getSource());
            if (source == null) {
                source = new Vertex(route.getSource());
                referencesCache.put(route.getSource(), source);
            }
            Collection<Edge> edges = vertices.get(source);
            if (edges == null) {
                edges = new ArrayList<>();
                vertices.put(source, edges);
            }
            Vertex destination = referencesCache.get(route.getDestination());
            if (destination == null) {
                destination = new Vertex(route.getDestination());
                referencesCache.put(route.getDestination(), destination);
            }
            final Edge edge = new Edge(destination, route.getDistance());
            edges.add(edge);
        }
        //normalize map
        for (final Map.Entry<Vertex, Collection<Edge>> entry : vertices.entrySet()) {
            final Vertex source = entry.getKey();
            final Collection<Edge> edges = entry.getValue();
            source.setAdjacencies(new ArrayList<>(edges));
        }
        final Set<Vertex> retval = new HashSet<>(vertices.keySet());
        //Get end paths
        Collection<Collection<Edge>> allEdges = vertices.values();
        for (Collection<Edge> edges : allEdges) {
            for (Edge edge : edges) {
                if (!retval.contains(edge.getTarget())) {
                    retval.add(edge.getTarget());
                }
            }
        }
        return retval;
    }

}
