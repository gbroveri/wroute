package br.com.wroute.service.it;

import br.com.wroute.domain.LogisticMap;
import br.com.wroute.domain.RouteCost;
import br.com.wroute.service.exception.BusinessException;

import java.math.BigDecimal;

/**
 * Created by gbroveri.
 */
public interface LogisticMapService {
    LogisticMap create(LogisticMap newMap) throws BusinessException;

    RouteCost findRouteCost(String source, String destination, Integer consumption, BigDecimal fuelPrice) throws BusinessException;
}
