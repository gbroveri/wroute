package br.com.wroute.service.it;

import br.com.wroute.domain.LogisticMap;
import br.com.wroute.domain.Route;

import java.util.Collection;

/**
 * Created by gbroveri.
 */
public interface LogisticMapDao {
    LogisticMap create(final LogisticMap map);

    void delete(final Object id);

    LogisticMap find(final Object id);

    LogisticMap update(final LogisticMap t);

    Collection<Route> findAllRoutes();
}
