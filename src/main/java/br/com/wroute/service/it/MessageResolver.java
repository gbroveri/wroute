package br.com.wroute.service.it;

/**
 * Created by gbroveri.
 */

import br.com.wroute.domain.AppError;
import br.com.wroute.domain.Domain;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;

import javax.validation.ConstraintViolation;
import java.util.Locale;

/**
 * Created by gbroveri.
 */
public interface MessageResolver {
    public String getMessage(String code, Object[] args, String defaultMessage, Locale locale);

    public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException;

    public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException;

    public String getMessage(ConstraintViolation<? extends Domain> error, Locale locale);

    public String getMessage(AppError error, Locale locale);
}

