package br.com.wroute.service;

/**
 * Created by gbroveri.
 */

import br.com.wroute.domain.AppError;
import br.com.wroute.domain.Domain;
import br.com.wroute.service.it.MessageResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import java.util.Locale;

/**
 * Created by gbroveri.
 */
@Component
public class MessageResolverImpl implements MessageResolver {
    @Autowired
    private MessageSource messageSource;

    public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
        return messageSource.getMessage(code, args, defaultMessage, locale);
    }

    public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
        return messageSource.getMessage(code, args, locale);
    }

    public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
        return messageSource.getMessage(resolvable, locale);
    }

    public String getMessage(ConstraintViolation<? extends Domain> error, Locale locale) {
        return messageSource.getMessage(error.getMessageTemplate(), error.getExecutableParameters(), locale);
    }

    public String getMessage(AppError error, Locale locale) {
        String message = error.getMessage();
        try {
            message = messageSource.getMessage(error.getCode(), error.getParams(), locale);
        } catch (NoSuchMessageException e) {
            //
        }
        return message;
    }
}

