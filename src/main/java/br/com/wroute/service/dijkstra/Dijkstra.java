package br.com.wroute.service.dijkstra;


import java.util.*;

/**
 * Created by gbroveri.
 */
public class Dijkstra {

    private final Collection<Vertex> vertices;

    public Dijkstra(Collection<Vertex> vertices) {
        this.vertices = vertices;
    }

    public void loadDistances(String source) {
        Vertex sourceVertex = getVertex(source);

        if (sourceVertex == null)
            return;

        sourceVertex.setMinDistance(0);
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
        vertexQueue.add(sourceVertex);

        while (!vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.poll();
            if (u.getAdjacencies() != null) {
                for (Edge e : u.getAdjacencies()) {
                    Vertex v = e.getTarget();
                    Integer weight = e.getWeight();
                    Integer distanceThroughU = u.getMinDistance() + weight;
                    if (distanceThroughU < v.getMinDistance()) {
                        vertexQueue.remove(v);
                        v.setMinDistance(distanceThroughU);
                        v.setPrevious(u);
                        vertexQueue.add(v);
                    }
                }
            }
        }
    }

    public List<Vertex> getShortestPathTo(String target) {
        Vertex targetVertex = getVertex(target);
        if (targetVertex == null)
            return null;
        List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = targetVertex; vertex != null; vertex = vertex.getPrevious()) {
            path.add(vertex);
        }
        Collections.reverse(path);
        return path;
    }

    private Vertex getVertex(String name) {
        Vertex targetVertex = null;
        for (Vertex vertex : this.vertices) {
            if (name.equals(vertex.getName())) {
                targetVertex = vertex;
                break;
            }
        }
        return targetVertex;
    }

}
