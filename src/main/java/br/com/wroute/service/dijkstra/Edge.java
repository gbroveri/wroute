package br.com.wroute.service.dijkstra;

/**
 * Created by gbroveri.
 */
public class Edge {
    private final Vertex target;
    private final Integer weight;

    public Edge(Vertex pTarget, Integer pWeight) {
        target = pTarget;
        weight = pWeight;
    }

    public Vertex getTarget() {
        return target;
    }

    public Integer getWeight() {
        return weight;
    }
}
