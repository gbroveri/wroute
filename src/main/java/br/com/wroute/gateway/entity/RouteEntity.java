package br.com.wroute.gateway.entity;

import javax.persistence.*;

/**
 * Created by gbroveri.
 */
@Entity
@Table(name = "route")
public class RouteEntity implements WallRouteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "source")
    private String source;

    @Column(name = "destination")
    private String destination;

    @Column(name = "distance")
    private Integer distance;

    @ManyToOne
    @JoinColumn(name = "map_id", nullable = false)
    private LogisticMapEntity map;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public LogisticMapEntity getMap() {
        return map;
    }

    public void setMap(LogisticMapEntity map) {
        this.map = map;
    }
}
