package br.com.wroute.gateway.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by gbroveri.
 */
@Entity
@Table(name = "logistic_map")
public class LogisticMapEntity implements WallRouteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "map")
    private Set<RouteEntity> routes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<RouteEntity> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<RouteEntity> routes) {
        this.routes = routes;
    }

    public void addToRoutes(RouteEntity route) {
        if (routes == null)
            routes = new HashSet<>();
        route.setMap(this);
        routes.add(route);
    }
}
