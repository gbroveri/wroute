package br.com.wroute.gateway.controller.serializer.it;

import br.com.wroute.domain.LogisticMap;

/**
 * Created by gbroveri.
 */
public interface LogisticMapSerializer extends JsonSerializer<LogisticMap> {
}
