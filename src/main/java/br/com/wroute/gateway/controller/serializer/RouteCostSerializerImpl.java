package br.com.wroute.gateway.controller.serializer;

import br.com.wroute.domain.RouteCost;
import br.com.wroute.gateway.controller.serializer.it.RouteCostSerializer;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.springframework.stereotype.Component;

/**
 * Created by gbroveri.
 */
@Component
public class RouteCostSerializerImpl implements RouteCostSerializer {
    @Override
    public String serialize(RouteCost domain) {
        return new JSONSerializer().exclude("*.class", "*.id").deepSerialize(domain);
    }

    @Override
    public RouteCost deserialize(String json) {
        return new JSONDeserializer<RouteCost>().deserialize(json, RouteCost.class);
    }

}
