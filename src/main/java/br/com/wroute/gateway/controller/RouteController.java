package br.com.wroute.gateway.controller;

import br.com.wroute.domain.LogisticMap;
import br.com.wroute.domain.Route;
import br.com.wroute.domain.RouteCost;
import br.com.wroute.gateway.controller.serializer.it.LogisticMapSerializer;
import br.com.wroute.gateway.controller.serializer.it.RouteCostSerializer;
import br.com.wroute.service.exception.BusinessException;
import br.com.wroute.service.it.LogisticMapService;
import br.com.wroute.service.it.MessageResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by gbroveri.
 */
@Controller
@RequestMapping("/api/routes")
public class RouteController {

    @Autowired
    private LogisticMapService mapService;

    @Autowired
    private MessageResolver messageResolver;

    @Autowired
    private LogisticMapSerializer mapSerializer;

    @Autowired
    private RouteCostSerializer costSerializer;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> createRoute(@RequestBody String json, HttpServletRequest request) throws BusinessException {
        LogisticMap map;
        if (json != null && !"".equals(json)) {
            map = mapSerializer.deserialize(json);
            mapService.create(map);
            final HttpHeaders headers = getJsonHeader();
            return new ResponseEntity<>(messageResolver.getMessage("default.create", null, request.getLocale()), headers, HttpStatus.OK);
        } else {
            map = getTemplateMap();
            final HttpHeaders headers = getJsonHeader();
            return new ResponseEntity<>(mapSerializer.serialize(map), headers, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getBestRoute(
            @RequestParam(value = "source", required = true) String source,
            @RequestParam(value = "destination", required = true) String destination,
            @RequestParam(value = "consumption", required = true) Integer consumption,
            @RequestParam(value = "fuelPrice", required = true) @NumberFormat(pattern = "###.###,##") BigDecimal fuelPrice
    ) throws BusinessException {
        final HttpHeaders headers = getJsonHeader();
        RouteCost routeCost = mapService.findRouteCost(source, destination, consumption, fuelPrice);
        return new ResponseEntity<>(costSerializer.serialize(routeCost), headers, HttpStatus.OK);
    }

    private HttpHeaders getJsonHeader() {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        return headers;
    }

    private LogisticMap getTemplateMap() {
        LogisticMap map;
        map = new LogisticMap();
        map.setName("template");
        Route route = new Route();
        route.setDistance(10);
        route.setSource("A");
        route.setDestination("B");
        Collection<Route> routes = new ArrayList<>();
        routes.add(route);
        map.setRoutes(routes);
        return map;
    }


}
