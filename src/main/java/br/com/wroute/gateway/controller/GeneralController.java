package br.com.wroute.gateway.controller;

import br.com.wroute.domain.AppError;
import br.com.wroute.service.exception.BusinessException;
import br.com.wroute.service.it.MessageResolver;
import flexjson.JSONSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Created by gbroveri.
 */
@ControllerAdvice
public class GeneralController {
    @Autowired
    MessageResolver messageResolver;

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<String> handleBusinessException(Locale locale, BusinessException e) {
        final Set<AppError> errors = new HashSet<>();
        for (AppError error : e.getErrors()) {
            error.setMessage(messageResolver.getMessage(error, locale));
            errors.add(error);
        }
        final String json = new JSONSerializer().exclude("*.class", "*.code").serialize(errors);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<>(json, headers, HttpStatus.BAD_REQUEST);
    }

}

