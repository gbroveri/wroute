package br.com.wroute.gateway.controller.serializer.it;

import br.com.wroute.domain.Domain;

/**
 * Created by gbroveri.
 */
public interface JsonSerializer<T extends Domain> {
    String serialize(T domain);

    T deserialize(String json);
}
