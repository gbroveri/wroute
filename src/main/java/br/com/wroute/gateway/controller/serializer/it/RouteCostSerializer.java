package br.com.wroute.gateway.controller.serializer.it;

import br.com.wroute.domain.RouteCost;

/**
 * Created by gbroveri.
 */
public interface RouteCostSerializer extends JsonSerializer<RouteCost> {
}
