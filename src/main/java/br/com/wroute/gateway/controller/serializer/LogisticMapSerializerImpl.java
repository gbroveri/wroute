package br.com.wroute.gateway.controller.serializer;

import br.com.wroute.domain.LogisticMap;
import br.com.wroute.gateway.controller.serializer.it.LogisticMapSerializer;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.springframework.stereotype.Component;

/**
 * Created by gbroveri.
 */
@Component
public class LogisticMapSerializerImpl implements LogisticMapSerializer {
    @Override
    public String serialize(LogisticMap domain) {
        return new JSONSerializer().exclude("*.class", "*.id", "*.mapId").deepSerialize(domain);
    }

    @Override
    public LogisticMap deserialize(String json) {
        return new JSONDeserializer<LogisticMap>().deserialize(json, LogisticMap.class);
    }
}
