package br.com.wroute.gateway.converter;

import br.com.wroute.domain.LogisticMap;
import br.com.wroute.gateway.converter.it.LogisticMapConverter;
import br.com.wroute.gateway.converter.it.RouteConverter;
import br.com.wroute.gateway.entity.LogisticMapEntity;
import br.com.wroute.gateway.entity.RouteEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by gbroveri.
 */
@Component
public class LogisticMapConverterImpl implements LogisticMapConverter {

    @Autowired
    private RouteConverter routeConverter;

    @Override
    public LogisticMap convertToDomain(LogisticMapEntity entity) {
        LogisticMap domain = new LogisticMap();
        domain.setId(entity.getId());
        domain.setName(entity.getName());
        domain.setRoutes(routeConverter.convertToDomains(entity.getRoutes()));
        return domain;
    }

    @Override
    public LogisticMapEntity convertToEntity(LogisticMap domain) {
        LogisticMapEntity entity = new LogisticMapEntity();
        entity.setId(domain.getId());
        entity.setName(domain.getName());
        for (RouteEntity routeEntity : routeConverter.convertToEntities(domain.getRoutes()))
            entity.addToRoutes(routeEntity);
        return entity;
    }

    @Override
    public Collection<LogisticMap> convertToDomains(Collection<LogisticMapEntity> entities) {
        final Collection<LogisticMap> result = new ArrayList<>();
        for (LogisticMapEntity entity : entities) {
            result.add(this.convertToDomain(entity));
        }
        return result;

    }

    @Override
    public Collection<LogisticMapEntity> convertToEntities(Collection<LogisticMap> domains) {
        final Collection<LogisticMapEntity> result = new ArrayList<>();
        for (LogisticMap domain : domains) {
            result.add(this.convertToEntity(domain));
        }
        return result;
    }
}
