package br.com.wroute.gateway.converter;

import br.com.wroute.domain.Route;
import br.com.wroute.gateway.converter.it.RouteConverter;
import br.com.wroute.gateway.entity.LogisticMapEntity;
import br.com.wroute.gateway.entity.RouteEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by gbroveri.
 */
@Component
public class RouteConverterImpl implements RouteConverter {
    @Override
    public Route convertToDomain(RouteEntity entity) {
        Route domain = new Route();
        domain.setId(entity.getId());
        domain.setMapId(entity.getMap().getId());
        domain.setSource(entity.getSource());
        domain.setDestination(entity.getDestination());
        domain.setDistance(entity.getDistance());
        return domain;
    }

    @Override
    public RouteEntity convertToEntity(Route domain) {
        RouteEntity entity = new RouteEntity();
        entity.setId(domain.getId());
        entity.setSource(domain.getSource());
        entity.setDestination(domain.getDestination());
        entity.setDistance(domain.getDistance());
        LogisticMapEntity mapEntity = new LogisticMapEntity();
        mapEntity.setId(domain.getMapId());
        entity.setMap(mapEntity);
        return entity;
    }

    @Override
    public Collection<Route> convertToDomains(Collection<RouteEntity> entities) {
        final Collection<Route> result = new ArrayList<>();
        for (RouteEntity entity : entities) {
            result.add(this.convertToDomain(entity));
        }
        return result;
    }

    @Override
    public Collection<RouteEntity> convertToEntities(Collection<Route> domains) {
        final Collection<RouteEntity> result = new ArrayList<>();
        for (Route domain : domains) {
            result.add(this.convertToEntity(domain));
        }
        return result;
    }
}
