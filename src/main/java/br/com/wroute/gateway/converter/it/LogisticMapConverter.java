package br.com.wroute.gateway.converter.it;

import br.com.wroute.domain.LogisticMap;
import br.com.wroute.gateway.entity.LogisticMapEntity;

/**
 * Created by gbroveri.
 */
public interface LogisticMapConverter extends Converter<LogisticMapEntity, LogisticMap> {
}
