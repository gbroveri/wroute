package br.com.wroute.gateway.converter.it;

import br.com.wroute.domain.Route;
import br.com.wroute.gateway.entity.RouteEntity;

/**
 * Created by gbroveri.
 */
public interface RouteConverter extends Converter<RouteEntity, Route> {
}
