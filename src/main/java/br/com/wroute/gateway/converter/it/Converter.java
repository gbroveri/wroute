package br.com.wroute.gateway.converter.it;

import br.com.wroute.domain.Domain;
import br.com.wroute.gateway.entity.WallRouteEntity;

import java.util.Collection;

/**
 * Created by gbroveri.
 */
public interface Converter<E extends WallRouteEntity, D extends Domain> {
    D convertToDomain(final E entity);

    E convertToEntity(final D domain);

    Collection<D> convertToDomains(final Collection<E> entities);

    Collection<E> convertToEntities(final Collection<D> domains);
}
