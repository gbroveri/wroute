package br.com.wroute.gateway.repository;

import br.com.wroute.domain.LogisticMap;
import br.com.wroute.domain.Route;
import br.com.wroute.gateway.converter.it.LogisticMapConverter;
import br.com.wroute.gateway.converter.it.RouteConverter;
import br.com.wroute.gateway.entity.LogisticMapEntity;
import br.com.wroute.gateway.entity.RouteEntity;
import br.com.wroute.service.it.LogisticMapDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;

/**
 * Created by gbroveri.
 */
@Repository
public class LogisticMapJpaDao implements LogisticMapDao {

    @PersistenceContext
    EntityManager em;

    @Autowired
    LogisticMapConverter mapConverter;

    @Autowired
    RouteConverter routeConverter;

    @Transactional
    public LogisticMap create(final LogisticMap map) {
        final LogisticMapEntity mapEntity = mapConverter.convertToEntity(map);
        resolveRefs(mapEntity);
        this.em.persist(mapEntity);
        return mapConverter.convertToDomain(mapEntity);
    }

    public void delete(final Object id) {
        this.em.remove(this.em.getReference(LogisticMapEntity.class, id));
    }

    public LogisticMap find(final Object id) {
        final LogisticMapEntity mapEntity = this.em.find(LogisticMapEntity.class, id);
        return mapConverter.convertToDomain(mapEntity);
    }

    public LogisticMap update(final LogisticMap t) {
        final LogisticMapEntity mapEntity = mapConverter.convertToEntity(t);
        resolveRefs(mapEntity);
        this.em.merge(t);
        return mapConverter.convertToDomain(mapEntity);
    }

    public Collection<Route> findAllRoutes() {
        StringBuilder sb = new StringBuilder("from ")
                .append(RouteEntity.class.getName());
        Query query = em.createQuery(sb.toString());
        final Collection<RouteEntity> entities = (Collection<RouteEntity>) query.getResultList();
        return routeConverter.convertToDomains(entities);
    }

    private void resolveRefs(LogisticMapEntity entity) {
        for (RouteEntity routeEntity : entity.getRoutes()) {
            routeEntity.setMap(entity);
        }
    }
}
