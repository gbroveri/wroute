@txn
Feature: Manage logistic maps
  As logistics operator
  I need to manage the logistics maps
  So that I can get optimized routes minimizing costs of operation

  Scenario: create a valid map on Wroute
    When logistic operator saves map "sp/interior/capital" with routes Campinas -> Valinhos 20km, Valinhos -> Vinhedo 10km, Vinhedo -> São Paulo 70km
      | source   | destination | distance |
      | Campinas | Valinhos    | 20       |
      | Valinhos | Vinhedo     | 10       |
      | Vinhedo  | São Paulo   | 70       |
    Then http status 200 is returned success message "Successfully created."

  Scenario: create a map without a name
    When logistic operator tries to save a map without name with routes Campinas->Valinhos 20km
      | source   | destination | distance |
      | Campinas | Valinhos    | 20       |
    Then http status 400 is returned with error message "Name is required."

  Scenario: create a map with no routes
    When logistic operator tries to save a map named "route 66" with no routes
    Then http status 400 is returned with error message "Routes must not be empty."

  Scenario: create a map with route that has no "source"
    When logistic operator tries to save a map named "route 66" with no source to California
      | source | destination | distance |
      |        | California  | 3000     |
    Then http status 400 is returned with error message "Source is required."

  Scenario: create a map with route that has no "destination"
    When logistic operator tries to save a map named "route 66" from Chicago without destination
      | source  | destination | distance |
      | Chicago |             | 3000     |
    Then http status 400 is returned with error message "Destination is required."

  Scenario: create a map with route that has no distance
    When logistic operator tries to save a map named "route 66" from Chicago to California without distance
      | source  | destination | distance |
      | Chicago | California  |          |
    Then http status 400 is returned with error message "Distance is required."

  Scenario: create a map with route that has distance 0km
    When logistic operator tries to save a map named "route 66" from Chicago to California without distance
      | source  | destination | distance |
      | Chicago | California  | 0        |
    Then http status 400 is returned with error message "Distance must be greater than 0."

  Scenario: create a map with route that has "negative distance"
    When logistic operator tries to save a map named "route 66" from Chicago to California with distance [-3000]
      | source  | destination | distance |
      | Chicago | California  | -3000    |
    Then http status 400 is returned with error message "Distance must be greater than 0."

  Scenario: create 2 maps with same name
    Given exists map "route 66" from Chicago to California
      | source  | destination | distance |
      | Chicago | California  | 3000     |
    When logistic operator tries to include new map "route 66" again
      | source  | destination | distance |
      | Chicago | California  | 3000     |
    Then http status 400 is returned with error message "Map name must be unique."

  Scenario: find best cost of a route
    Given exists "given" map with routes
      | source | destination | distance |
      | A      | B           | 10       |
      | B      | D           | 15       |
      | A      | C           | 20       |
      | C      | D           | 30       |
      | B      | E           | 50       |
      | D      | E           | 30       |
    When logistic operator asks for the cost of source "A" to destination "D" with fuel consumption "10" km/l, fuel price "2.50"/liter.
    Then wroute returns route "A -> B -> D" with cost "6.25"

  Scenario: create same source and destination route
    When logistic operator tries to save map "chicago" from Chicago to Chicago with distance 1km
      | source  | destination | distance |
      | Chicago | Chicago     | 1        |
    Then http status 400 is returned with error message "Source and Destination must not be the same."

  Scenario: asking for source that does not exist
    Given exists route A -> C with distance of 1km
      | source | destination | distance |
      | A      | C           | 1        |
    When logistic operator asks for cost from "B" to "C"
    Then http status 400 is returned with error message "Route not found."

  Scenario: asking for destination that does not exist
    Given exists route A -> C with distance of 1km
      | source | destination | distance |
      | A      | C           | 1        |
    When logistic operator asks for cost from "A" to "B"
    Then http status 400 is returned with error message "Route not found."

  Scenario: there is no connection between source and destination
    Given exists routes A -> B, B -> D and C -> D
      | source | destination | distance |
      | A      | B           | 1        |
      | B      | D           | 1        |
      | C      | D           | 1        |
    When logistic operator asks for cost from "A" to "C"
    Then http status 400 is returned with error message "Route not found."

  Scenario: there are 2 routes for same source and destination
    Given exists map "map A" with routes A -> B 10km, B -> C 10km
      | source | destination | distance |
      | A      | B           | 10       |
      | B      | C           | 10       |
    And exists map "map B" with route A -> B 5km
      | source | destination | distance |
      | A      | B           | 5        |
    When logistic operator asks for the cost of source "A" to destination "C" with fuel consumption "10" km/l, fuel price "2.50"/liter.
    Then wroute returns route "A -> B -> C" with cost "3.75"

  Scenario: post without a map on body
    When logistic operator posts on rest api without json body
    Then http status 400 is returned with template body

  Scenario Outline: test cost calculation
    Given <distance>km distance, <consumption>km/l consumption and $<fuelPrice> fuel price
    Then assert cost <expectedCost>
  Examples:
    | distance | consumption | fuelPrice | expectedCost |
    | 100      | 10          | 3.00      | 30.00        |
    | 50       | 8           | 3.20      | 20.00        |
    | 25       | 10          | 2.50      | 6.25         |
    | 25       | 10          | 2.99      | 7.47         |