package br.com.wroute;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by gbroveri.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        format = {"json", "json:target/cucumber.json"},
        features = {"."}, strict = true)
public class RunCakesTest {
}
