package br.com.wroute.service;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;

/**
 * Created by gbroveri.
 */
public class LogisticMapTest {

    LogisticMapServiceImpl service = new LogisticMapServiceImpl();
    BigDecimal cost;

    @Given("^(\\d+)km distance, (\\d+)km/l consumption and \\$(\\d+)\\.(\\d+) fuel price$")
    public void km_distance_km_l_consumption_and_$_fuel_price(int distance, int consumption, int integer, int decimal) throws Throwable {
        BigDecimal fuelPrice = BigDecimal.valueOf(Double.valueOf("" + integer + "." + decimal));
        cost = service.calculateCost(distance, consumption, fuelPrice).setScale(2, BigDecimal.ROUND_FLOOR);
    }

    @Then("^assert cost (\\d+)\\.(\\d+)$")
    public void assert_cost(int integer, int decimal) throws Throwable {
        BigDecimal expectedCost = BigDecimal.valueOf(Double.valueOf("" + integer + "." + decimal)).setScale(2, BigDecimal.ROUND_FLOOR);
        assertTrue("Assertion failed: expected [" + expectedCost + "] found [" + cost + "]", expectedCost.equals(cost));
    }

}
