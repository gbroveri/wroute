package br.com.wroute.service;

import br.com.wroute.config.ApplicationConfig;
import br.com.wroute.domain.LogisticMap;
import br.com.wroute.domain.Route;
import br.com.wroute.domain.RouteCost;
import br.com.wroute.mock.SpringMockTestConfig;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.spring.SpringTransactionHooks;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by gbroveri.
 */
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, SpringMockTestConfig.class})
public class LogisticMapIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    SpringTransactionHooks transactionHooks;

    private MvcResult result;

    @Before(value = {"@txn"}, order = 100)
    public void startTransaction() {
        transactionHooks.startTransaction();
    }

    @After(value = {"@txn"}, order = 100)
    public void rollBackTransaction() {
        transactionHooks.rollBackTransaction();
    }

    @When("^logistic operator saves map \"(.*?)\" with routes Campinas -> Valinhos (\\d+)km, Valinhos -> Vinhedo (\\d+)km, Vinhedo -> São Paulo (\\d+)km$")
    public void logistic_operator_saves_map_with_routes_Campinas_Valinhos_km_Valinhos_Vinhedo_km_Vinhedo_São_Paulo_km(String mapName, int arg2, int arg3, int arg4, List<Route> routes) throws Throwable {
        postExpectingSuccess(mapName, routes);
    }

    @Then("^http status (\\d+) is returned with error message \"(.*?)\"$")
    public void http_status_is_returned_with_error_message(int arg1, String arg2) throws Throwable {
        assertTrue(result.getResponse().getStatus() == arg1);
        final String response = result.getResponse().getContentAsString();
        assertTrue(response, response.contains(arg2));
    }

    @Then("^http status (\\d+) is returned success message \"(.*?)\"$")
    public void http_status_is_returned_success_message(int arg1, String arg2) throws Throwable {
        assertTrue(result.getResponse().getContentAsString(), result.getResponse().getContentAsString().contains("Successfully created."));
    }

    @When("^logistic operator tries to save a map without name with routes Campinas->Valinhos (\\d+)km$")
    public void logistic_operator_tries_to_save_a_map_without_name_with_routes_Campinas_Valinhos_km(int arg1, List<Route> routes) throws Throwable {
        postExpectingConstraintError("", routes);
    }

    @When("^logistic operator tries to save a map named \"(.*?)\" with no routes$")
    public void logistic_operator_tries_to_save_a_map_named_with_no_routes(String mapName) throws Throwable {
        postExpectingConstraintError(mapName, null);
    }

    @When("^logistic operator tries to save a map named \"(.*?)\" with no source to California$")
    public void logistic_operator_tries_to_save_a_map_named_with_no_source_to_California(String mapName, List<Route> routes) throws Throwable {
        postExpectingConstraintError(mapName, routes);
    }

    @When("^logistic operator tries to save a map named \"(.*?)\" from Chicago without destination$")
    public void logistic_operator_tries_to_save_a_map_named_from_Chicago_without_destination(String mapName, List<Route> routes) throws Throwable {
        postExpectingConstraintError(mapName, routes);
    }

    @When("^logistic operator tries to save a map named \"(.*?)\" from Chicago to California without distance$")
    public void logistic_operator_tries_to_save_a_map_named_from_Chicago_to_California_without_distance(String mapName, List<Route> routes) throws Throwable {
        postExpectingConstraintError(mapName, routes);
    }

    @When("^logistic operator tries to save a map named \"(.*?)\" from Chicago to California with distance \\[-(\\d+)\\]$")
    public void logistic_operator_tries_to_save_a_map_named_from_Chicago_to_California_with_distance(String mapName, int arg2, List<Route> routes) throws Throwable {
        postExpectingConstraintError(mapName, routes);
    }

    @When("^logistic operator tries to include new map \"(.*?)\" again$")
    public void logistic_operator_tries_to_include_new_map(String mapName, List<Route> routes) throws Throwable {
        postExpectingConstraintError(mapName, routes);
    }

    @When("^logistic operator asks for the cost of source \"(.*?)\" to destination \"(.*?)\" with fuel consumption \"(.*?)\" km/l, fuel price \"(.*?)\"/liter\\.$")
    public void logistic_operator_asks_for_the_cost_of_source_to_destination_with_fuel_consumption_km_l_fuel_price_liter(String arg1, String arg2, String arg3, String arg4) throws Throwable {
        result = mockMvc.perform(
                get("/api/routes")
                        .param("source", arg1)
                        .param("destination", arg2)
                        .param("consumption", arg3)
                        .param("fuelPrice", arg4)
        )
                .andExpect(status().isOk()).andReturn();
    }

    @When("^logistic operator tries to save map \"(.*?)\" from Chicago to Chicago with distance (\\d+)km$")
    public void logistic_operator_tries_to_save_map_from_Chicago_to_Chicago_with_distance_km(String mapName, int distance, List<Route> routes) throws Throwable {
        postExpectingConstraintError(mapName, routes);
    }

    @Given("^exists route A -> C with distance of (\\d+)km$")
    public void exists_route_A_C_with_distance_of_km(int arg1, List<Route> routes) throws Throwable {
        postExpectingSuccess("teste", routes);
    }

    @When("^logistic operator asks for cost from \"(.*?)\" to \"(.*?)\"$")
    public void logistic_operator_asks_for_cost_from_to(String arg1, String arg2) throws Throwable {
        result = mockMvc.perform(
                get("/api/routes")
                        .param("source", arg1)
                        .param("destination", arg2)
                        .param("consumption", "10")
                        .param("fuelPrice", "1.00")
        )
                .andExpect(status().is4xxClientError()).andReturn();
    }

    @Given("^exists routes A -> B, B -> D and C -> D$")
    public void exists_routes_A_B_B_D_and_C_D(List<Route> routes) throws Throwable {
        postExpectingSuccess("teste", routes);
    }

    @Given("^exists map \"(.*?)\" from Chicago to California$")
    public void exists_map_from_Chicago_to_California(String mapName, List<Route> routes) throws Throwable {
        postExpectingSuccess(mapName, routes);
    }

    @Given("^exists \"(.*?)\" map with routes$")
    public void exists_map_with_routes(String mapName, List<Route> routes) throws Throwable {
        postExpectingSuccess(mapName, routes);
    }


    private void postExpectingConstraintError(String mapName, List<Route> routes) throws Exception {
        LogisticMap logisticMap = new LogisticMap(mapName, routes);
        String json = new JSONSerializer().exclude("*.class").deepSerialize(logisticMap);
        result = mockMvc.perform(
                post("/api/routes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().is4xxClientError()).andReturn();
    }

    private void postExpectingSuccess(String mapName, List<Route> routes) throws Exception {
        LogisticMap logisticMap = new LogisticMap(mapName, routes);
        String json = new JSONSerializer().exclude("*.class").deepSerialize(logisticMap);
        result = mockMvc.perform(
                post("/api/routes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().isOk()).andReturn();
    }

    @Then("^wroute returns route \"(.*?)\" with cost \"(.*?)\"$")
    public void wroute_returns_route_with_cost(String arg1, String arg2) throws Throwable {
        RouteCost cost = new JSONDeserializer<RouteCost>().deserialize(result.getResponse().getContentAsString(), RouteCost.class);
        assertTrue(cost.getValue().equals(BigDecimal.valueOf(Double.valueOf(arg2))));
    }

    @Given("^exists map \"(.*?)\" with routes A -> B (\\d+)km, B -> C (\\d+)km$")
    public void exists_map_with_routes_A_B_km_B_C_km(String mapName, int arg2, int arg3, List<Route> routes) throws Throwable {
        postExpectingSuccess(mapName, routes);
    }

    @Given("^exists map \"(.*?)\" with route A -> B (\\d+)km$")
    public void exists_map_with_route_A_B_km(String mapName, int arg2, List<Route> routes) throws Throwable {
        postExpectingSuccess(mapName, routes);
    }

    @When("^logistic operator posts on rest api without json body$")
    public void logistic_operator_posts_on_rest_api_without_json_body() throws Throwable {
        result = mockMvc.perform(
                post("/api/routes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("")
        )
                .andExpect(status().is4xxClientError()).andReturn();
    }

    @Then("^http status (\\d+) is returned with template body$")
    public void http_status_is_returned_with_template_body(int arg1) throws Throwable {
        String json = result.getResponse().getContentAsString();
        LogisticMap map = new JSONDeserializer<LogisticMap>().deserialize(json, LogisticMap.class);
        assertTrue("template".equals(map.getName()));
    }

}
