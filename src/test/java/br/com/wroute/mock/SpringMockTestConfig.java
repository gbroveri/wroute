package br.com.wroute.mock;

import cucumber.api.spring.SpringTransactionHooks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


/**
 * Created by gbroveri.
 */
@Configuration
public class SpringMockTestConfig {
    @Autowired
    private WebApplicationContext wac;

    @Bean
    @Scope("prototype")
    public MockMvc getMockMvc() {
        return webAppContextSetup(wac).build();
    }

    @Bean
    @DependsOn("transactionManager")
    public SpringTransactionHooks springTransactionHooks() {
        SpringTransactionHooks springTransactionHooks = new SpringTransactionHooks();
        springTransactionHooks.setTxnManagerBeanName("transactionManager");
        return springTransactionHooks;
    }
}
